# Write four classes that meet these requirements.
#
# Name:       Animal
class Animal:
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
    def __init___(self, number_of_legs, primary_color):
        self.number_of_legs = number_of_legs
        self.primary_color = primary_color
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
    def Behavior(self):
        return f"{self.__class__name__} has {self.number_of_legs} legs and is primarily {self.primary_color}"
#
# Name:       Dog, inherits from Animal
class Dog(Animal):
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
    def speak(self):
        return "Bark!"
#
#
# Name:       Cat, inherits from Animal
class Cat(Animal):
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
    def speak(self):
        return "Miao!"
#
#
# Name:       Snake, inherits from Animal
class Snake(Animal):
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
    def speak(self):
        return "Ssssssss!"
