# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):

#   create list to hold keys
    keys_to_values = list(dictionary.keys())
#   create list to hold values
    values_to_keys = []
    #empty dictionary
    reversed_dict = {}
    #for loop to get values from dictionary using keys list
    for value in keys_to_values:
        values_to_keys.append(dictionary[value])

    #for loop to create dictionary switching values and keys
    for i in range(0, len(keys_to_values)):
        reversed_dict[values_to_keys[i]] = keys_to_values[i]

    return reversed_dict

print(reverse_dictionary({"one": 1, "two": 2, "three": 3}))
