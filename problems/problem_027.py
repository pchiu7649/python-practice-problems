# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max = 0
    #if statement to check for empty list
    if len(values) == 0:
        #return None
        return None
    #for loop to instantiate through list
    for num in values:
        #check if current number is largest
        if num > max:
            #store number
            max = num
    #return max value
    return max


list1 = [1,5,8,3,6,9,10,4,2,1]

print(max_in_list(list1))