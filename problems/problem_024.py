# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    #if statement to check if theres anything in the list
    if len(values) == 0:
        return None
    #for loop to determine average of the list
    for num in values:
        total += num

    average = total / len(values)

    return average

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list0 = []

print(calculate_average(list0))