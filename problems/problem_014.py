# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    #create list containing flour, eggs, and oil
    needed_ing = ['flour' ,'eggs', 'oil']
    #check if all ingredients are in the list
    check = all(item in ingredients for item in needed_ing)

    if check:
        #return true
        return True
    #return false
    return False

list1 = ['flour', 'eggs', 'fish']

print(can_make_pasta(list1))
