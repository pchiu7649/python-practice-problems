# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
#
# Example:
#    item = ReceiptItem(10, 3.45)
#
#    print(item.get_total())    # Prints 34.5


#create class ReceiptItem
class ReceiptItem:
    #define class with param (quantity, price)
    def __init__(self, quantity, price):
        #set variables to self.<variable>
        self.quantity = quantity
        self.price = price

    #define get_total
    def get_total(self):
        #return total price
        return self.quantity * self.price


item = ReceiptItem(10, 3.45)

print(item.get_total())
