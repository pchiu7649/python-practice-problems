# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    first_list = []
    second_list = []

    first_list_len = 0

    #determine if list is even or odd
    if len(list) % 2 == 0:
        first_list_len = int(len(list) / 2)
    else:
        first_list_len = int(len(list) /2) + 1

    for i in range(0, first_list_len):
        first_list.append(list[i])

    for i in range(first_list_len, len(list) ):
        second_list.append(list[i])

    return first_list, second_list

print(halve_the_list([1, 2, 3, 4]))