# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(number):

    fraction_string  = ""

    #populate fraction list depending on what is input
    for i in range (1, number + 1):
        if i == 1:
            fraction_string += f"{i}/{i+1}"
        else:
            fraction_string += f" + {i}/{i+1}"

    return fraction_string

print(sum_fraction_sequence(2))