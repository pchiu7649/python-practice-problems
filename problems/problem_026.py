# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    sum = 0

    #for statement to instantiate through the list
    for grades in values:

        sum += grades

    #calculate average
    average = sum / len(values)

    #if statement checks for 90+
    if average >= 90:
        return 'A'
    #elif checks for 80 - 89
    if average >= 80 and average <= 89:
        return 'B'
    #elif checks for 70-79
    if average >= 70 and average <= 79:
        return 'C'
    #elif checks for 60-69
    if average >= 60 and average <= 69:
        return 'D'
    #else F
    else:
        return 'F'

list1 = [ 60, 70, 0]

print(calculate_grade(list1))