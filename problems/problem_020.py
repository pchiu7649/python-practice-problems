# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    #get 50% of length of members list
    fifty_percent = len(members_list) * .5
    attendees = len(attendees_list)
    #compare attendees is greater than or equal to 50% of members
    if attendees >= fifty_percent:
        #true
        return True

    #false
    return False


people = ['fred', 'john']
members = ['fred','john','george','candace', 'karen']

print(has_quorum(people, members))