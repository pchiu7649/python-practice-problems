# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    #string to place letters in
    no_dupe = ""
    # instantiate through given string
    for char in s:
        # if the letter is not in the string
        if char not in no_dupe:
            # add it to the end of the string
            result += char

    # return finished string
    return no_dupe
