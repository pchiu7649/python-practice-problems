# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")
#
#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

#Create class Book
class Book:
    #Define book and have parameters(author_name, title)
    def __init__(self, author_name, title):
        #assign author_name to self.author_name
        self.author_name = author_name
        self.title = title
    #Define get author
    def get_author(self):
        #return "Author:" + author_name
        return "Author: " + self.author_name
    #Define get title
    def get_title(self):
        #return Title: + title
        return "Title: " + self.title

book = Book("Natalie Zina Walschots", "Hench")

print(book.get_author())  # prints "Author: Natalie Zina Walschots"
print(book.get_title())   # prints "Title: Hench"
