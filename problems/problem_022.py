# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    #create list to put gear
    gear = []
    #check if its a workday
    if is_workday:
        #add laptop
        gear.append('laptop')
        #check if it is not sunny
        if not is_sunny:
            #add umbrella
            gear.append('umbrella')
    #add surfboard
    else:
        gear.append('surfboard')

    return(gear)

work = False
sunny = True

print(gear_for_day(work, sunny))
