# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

#create function sum_two_numbers (x, y)
def sum_two_numbers(x, y):
    #return the sum of x and y
    return x + y


print(sum_two_numbers(3,4))
