# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    #create variables for first and second largest
    first = 0
    second = 0
    temp = 0

    #if statement to check if there's more 1 or less numbers in the list
    if len(values) <= 1:
        #return None
        return None

    #for loop instantiate through list
    for num in values:
        #if statement to check if the number is the biggest number
        if num > first:
            #save number to first largest hold current first number
            temp = first
            first = num
            #check to see if the old largest is now the second largest
            if temp > second:
                second = temp
        #elif to check if the number is bigger than the second biggest number
        elif num > second:
            #save number to sencond largest
            second = num

    #return second largest number
    return second

list1 = [1, 3, 5, 10, 8, 4]

print(find_second_largest(list1))