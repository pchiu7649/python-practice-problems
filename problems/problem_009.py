# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #create list and store word as list
    normal_word = []

    #reverse the word
    reverse_word = word[::-1]

    #compare word and reversed word
    if word == reverse_word:
        #return true
        return True
    #return false
    return False

print(is_palindrome('racecars'))