# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    #check if x and y are within bounds
    if (x >= 0 and x <= 10) and (y >= 0 and y <=10):
        #return true
        return True
    #return false
    return False

print(is_inside_bounds(4,12))
