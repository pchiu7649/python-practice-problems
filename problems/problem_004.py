# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    #make variable to store largest value
    large_value = 0
    #check if first value is larger or equal to
    if value1 >= value2:
        large_value = value1
    #if first value isn't larger or equal
    else:
        large_value = value2

     #then compare next two values
    if large_value >= value3:
        return large_value
    else:
        return large_value
