# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    #create variable to store answer
    sum = 0
    #if statement to check if limit < 0
    if n < 0:
        #return none
        return None

    #for to instantiate through 0 and limit
    for num in range(0,n+1):
        #multiply each number by 2 and add to sum
        sum += (num * 2)

    return sum

print(sum_of_first_n_even_numbers(5))