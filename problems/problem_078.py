# Write a class that meets these requirements.
#
# Name:       Circle
#
# Required state:
#    * radius, a non-negative value
#
# Behavior:
#    * calculate_perimeter()  # Returns the length of the perimater of the circle
#    * calculate_area()       # Returns the area of the circle
#
# Example:
#    circle = Circle(10)
#
#    print(circle.calculate_perimeter())  # Prints 62.83185307179586
#    print(circle.calculate_area())       # Prints 314.1592653589793
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

import math

#create Circle class
class Circle:
    #define circle class with param (radius)
    def __init__(self, radius):
        #check if radius is positive
        if radius < 0:
            #raise valueerror
            raise ValueError
        #set self.radius
        self.radius = radius
    #define calculate_perimeter
    def calculate_perimeter(self):
        # return perimeter (2piR)
        return 2 * math.pi * self.radius
    #define calculate_area
    def calculate_area(self):
        #return area (piRsquared)
        return math.pi * (self.radius ** 2)

circle = Circle(10)

print(circle.calculate_perimeter())
print(circle.calculate_area())