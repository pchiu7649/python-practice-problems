# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

#create class Student
class Student:
    #define Student with param(name)
    def __init__(self, name):
        #initiate self.name
        self.name = name
        #initiate empty list scores
        self.scores = []
    #define add_score
    def add_score(self, score):
        #add score to scores
        self.scores.append(score)
    #define get_average
    def get_average(self):
        #check if list is empty
        if len(self.scores) == 0:
            return None
        #return the average of scores
        return sum(self.scores) / len(self.scores)


student = Student("Malik")
#
print(student.get_average())    # Prints None
student.add_score(80)
print(student.get_average())    # Prints 80
student.add_score(90)
student.add_score(82)
print(student.get_average())    # Prints 84