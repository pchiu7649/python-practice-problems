# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    #variable for valid password
    lower = False
    upper = False
    digit = False
    special_char = False


    #check for at least one lowercase letter
    for char in password:
        if char.isalpha():
            if char.haslower():
                lower = True
            elif char.hasupper():
                upper = True
        elif char.isdigit():
            digit = True
        elif char == "!" or char == "$" or char == "@":
            special_char = True

    valid_pass = (len(password >= 6) and len(password <= 12)) and lower and upper and digit and special_char

    return valid_pass
