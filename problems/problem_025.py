# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum = 0

    #if statement to check if list is empty
    if len(values) == 0:
        return None
    #for loop to go through list and add
    for num in values:
        sum += num

    return sum

list1 = [1, 2, 3, 4, 5]
list0 = []

print(calculate_sum(list0))
